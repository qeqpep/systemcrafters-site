#+title: Hands-On Guile Scheme for Beginners

* Next Steps

Thank you so much for registering for the course!  I'm extremely excited to get started on this with you.

The most important thing to do is sign up for the discussion list for this course!  I wasn't able to get this process automated just yet so for now you'll need to put your e-mail address in this form:

#+BEGIN_EXPORT html

<div class="list-form center">
  <div class="list-form-title">Subscribe to the Course Discussion List!</div>
  <form method="POST" action="https://www.simplelists.com/subscribe.php">
    <input type="hidden" name="list" value="guile-beginners-0224@lists.systemcrafters.net">
    <input type="hidden" name="action" value="subscribe">
    <div class="list-form-message">
      This list will only be used for the purposes of this course!  It will be archived once the course is complete.
    </div>
    <div class="row">
      <div class="column">
        <div class="row center list-form-label">Display Name</div>
        <div class="row"><input type="text" name="name" /></div>
      </div>
      <div class="column">
        <div class="row center list-form-label">Email Address</div>
        <div class="row"><input type="text" name="email" /></div>
      </div>
    </div>
    <div>
      <input type="submit" value="Join the List"/>
    </div>
  </form>
</div>

#+END_EXPORT


I'll make sure that everyone is on the list before the course begins in case anyone misses this step.

* System Setup

Before the first meeting, you'll should try to you have a working Guile Scheme installation, preferrably with Emacs configured to use it.  This is not 100% necessary before the start date but will likely make it easier for you to participate from the first session.

*Full instructions coming soon!*  I will be producing a short guide (and hopefully a full SC video) on setting up Emacs for Scheme development with Guile.  Once that's done I will link it here!

* Course Schedule

This will be determined once we have a full group and I figure out what timezones we are working with!
